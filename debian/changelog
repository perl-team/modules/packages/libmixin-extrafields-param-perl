libmixin-extrafields-param-perl (0.022-1) unstable; urgency=medium

  * Remove internal comment from previous changelog entry.
  * Import upstream version 0.022.
  * Update years of packaging copyright.
  * Update upstream email address.
  * debian/copyright: drop second "Source".
  * Update debian/upstream/metadata.
  * Update alternative test dependency.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Jan 2023 20:20:37 +0100

libmixin-extrafields-param-perl (0.021-1) unstable; urgency=medium

  [ Florian Schlichting ]
  * Import Upstream version 0.021
  * Test::More is in libtest-simple-perl, stupid!
  * Add upstream-repo URL to Source field

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update GitHub URLs to use HTTPS.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Set Testsuite header for perl package.
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libtest-simple-perl and
      perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:35:25 +0100

libmixin-extrafields-param-perl (0.020-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 16:12:00 +0100

libmixin-extrafields-param-perl (0.020-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Florian Schlichting ]
  * Import Upstream version 0.020
  * Update upstream copyright statement
  * Update stand-alone license paragraphs to commonly used version (not
    limiting Debian to GNU/Linux, directly linking to GPL-1)
  * Bump dh compatibility to level 8 (no changes necessary)
  * Declare compliance with Debian Policy 3.9.4
  * Switch to source format 3.0 (quilt)
  * Add build-dependency on Test::More 0.96
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Thu, 03 Oct 2013 21:47:23 +0200

libmixin-extrafields-param-perl (0.011-2) unstable; urgency=low

  * Make (build) dependency on libmixin-extrafields-perl versioned
    (closes: #569437)

 -- gregor herrmann <gregoa@debian.org>  Fri, 12 Feb 2010 00:52:20 +0100

libmixin-extrafields-param-perl (0.011-1) unstable; urgency=low

  * Initial release (cf. #562102).

 -- gregor herrmann <gregoa@debian.org>  Wed, 23 Dec 2009 18:40:51 +0100
